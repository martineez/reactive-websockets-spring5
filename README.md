# Spring Tips: Reactive WebSockets with Spring Framework 5

This project is based on [blog](https://spring.io/blog/2017/09/27/spring-tips-reactive-websockets-with-spring-framework-5)

## Install Angular
`npm i -g @angular/cli`

## Install yarn (optional)
`npm i -g yarn`

## Register yarn package manager (optional)
```
yarn global add angular-cli
ng set --global packageManager=yarn
```

## Create Angular application 
`ng new ws-client`

## Run client
```
cd ws-client
ng serve --open
```

## Run service
```
mkdir ~\Desktop\in
cd ws-service
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
gradle sprintBoot
```
